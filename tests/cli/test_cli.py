"""Unit tests for the configsync.cli subpackage"""
import argparse

import pytest

import configsync.cli


@pytest.fixture
def LocalDB(mocker):
    db_mock = mocker.Mock(name="LocalDB_mock")
    mocker.patch("configsync.cli.LocalDatabase", mocker.Mock(return_value=db_mock))
    return db_mock


class TestParser:
    def test_add(self, LocalDB):

        configsync.cli.parse_args(["add", "-n", "SomeName", "-f", "SomeFile"])
        LocalDB.add.assert_called_with(app_name="SomeName", app_file="SomeFile")

    @pytest.mark.parametrize(
        "name_arg, name, file_arg, file",
        [
            pytest.param("--name", "SomeName", "--file", "SomeFile", id="long-version"),
            pytest.param("-n", "SomeName", "-f", "SomeFile", id="short-version"),
        ],
    )
    def test_remove_file(self, name_arg, name, file_arg, file, LocalDB):

        args = [name_arg, name, file_arg, file]
        configsync.cli.parse_args(["remove"] + args)
        LocalDB.remove.assert_called_with(app_name=name, app_file=file)

    def test_remove_app(self, LocalDB):

        configsync.cli.parse_args(["remove", "-n", "SomeName"])
        LocalDB.remove.assert_called_with(app_name="SomeName", app_file=None)

    @pytest.mark.parametrize(
        "name_arg, name",
        [
            pytest.param("--name", "SomeName", id="long-version"),
            pytest.param("-n", "SomeName", id="short-version"),
        ],
    )
    def test_list_app_files(self, name_arg, name, mocker):
        # We need MagicMock here because we use LocalDatabase.list in a for-loop
        LocalDB = mocker.MagicMock(name="LocalDB_mock")
        mocker.patch(
            "configsync.cli.utils.LocalDatabase", mocker.Mock(return_value=LocalDB)
        )
        configsync.cli.parse_args(["list"] + [name_arg, name])
        LocalDB.list.assert_called_with(app_name=name)

    def test_list_apps(self, mocker):
        # We need MagicMock here because we use LocalDatabase.list in a for-loop
        LocalDB = mocker.MagicMock(name="LocalDB_mock")
        mocker.patch(
            "configsync.cli.utils.LocalDatabase", mocker.Mock(return_value=LocalDB)
        )

        configsync.cli.parse_args(["list"])
        LocalDB.list.assert_called_with(app_name=None)

    @pytest.mark.parametrize(
        "remote_arg",
        [
            pytest.param("", id="default-(no-remote)"),
            pytest.param("--remote", id="to-remote-long-version"),
            pytest.param("-r", id="to-remote-short-version"),
        ],
    )
    def test_backup(self, remote_arg, LocalDB):

        configsync.cli.parse_args(
            ["backup"] + [remote_arg] if remote_arg else ["backup"]
        )
        LocalDB.backup.assert_called_with(to_remote=True if remote_arg else False)

    @pytest.mark.parametrize(
        "remote_arg",
        [
            pytest.param("", id="default-(no-remote)"),
            pytest.param("--remote", id="from-remote-long-version"),
            pytest.param("-r", id="from-remote-short-version"),
        ],
    )
    def test_restore(self, remote_arg, LocalDB):

        configsync.cli.parse_args(
            ["restore"] + [remote_arg] if remote_arg else ["restore"]
        )
        LocalDB.restore.assert_called_with(from_remote=True if remote_arg else False)

    @pytest.mark.parametrize("arg, val", [("token", "123"), ("repo", "https://some")])
    def test_config(self, arg, val, mocker):

        mocked_set = mocker.Mock(name="mocked_set")
        mocker.patch("configsync.cli.set_opt", mocked_set)

        configsync.cli.parse_args(f"config --{arg} {val}".split())

        args, kwargs = mocked_set.call_args
        assert arg, val in kwargs.items()


class TestCmdAction:
    def dummy_func(self, **args):
        return {k: str(v) + "_called" for k, v in args.items()}

    @pytest.fixture
    def single_action(self):
        return {
            "fire": {"action": self.dummy_func, "args": {"arg1": None, "arg2": None}}
        }

    @pytest.fixture
    def more_action(self, single_action):
        single_action.update(
            {
                "other": {
                    "action": self.dummy_func,
                    "args": {"arg1": None, "arg2": None, "arg3": None},
                }
            }
        )
        return single_action

    def test_add(self):

        act = configsync.cli.CmdAction(argparse.Namespace())
        act.add("fire", self.dummy_func, ["a", "b", "c"])

        expected = {
            "fire": {
                "action": self.dummy_func,
                "args": {"a": None, "b": None, "c": None},
            }
        }

        assert act.actions == expected

    def test_add_multiple(self):

        actions_list = [
            ("fire", self.dummy_func, ["arg1", "arg2"]),
            ("other", self.dummy_func, ["arg1", "arg2", "arg3"]),
        ]

        act = configsync.cli.CmdAction(argparse.Namespace())
        act.add_multiple(actions_list)

        expected = {
            "fire": {"action": self.dummy_func, "args": {"arg1": None, "arg2": None}},
            "other": {
                "action": self.dummy_func,
                "args": {"arg1": None, "arg2": None, "arg3": None},
            },
        }

        assert act.actions == expected

    @pytest.mark.parametrize(
        "cmd, parsed, expected",
        [
            (
                "fire",
                dict(cmd="fire", arg1=1, arg2=2),
                dict(arg1="1_called", arg2="2_called"),
            ),
            (
                "other",
                dict(cmd="fire", arg1="a", arg2="b", arg3="c"),
                dict(arg1="a_called", arg2="b_called", arg3="c_called"),
            ),
        ],
    )
    def test_do(self, cmd, parsed, expected, more_action):

        act = configsync.cli.CmdAction(argparse.Namespace(**parsed))

        act.actions = more_action

        assert act.do(cmd) == expected

    def test_get_parsed_arg_values(self, single_action):

        parsed = argparse.Namespace(**dict(cmd="fire", arg1=1, arg2=2))

        act = configsync.cli.CmdAction(parsed)

        act.actions = single_action

        cmd = act.actions.get("fire")
        ret = act._get_parsed_arg_values(cmd.get("args"))

        assert ret == {"arg1": 1, "arg2": 2}

"""Unit tests for the configsync.utils module"""
import configparser
import json

import pytest


import configsync.utils


@pytest.fixture(autouse=True)
def patch(module_patcher):
    module_patcher("utils")


@pytest.fixture
def mock_subprocess(mocker):
    mocked_subprocess = mocker.Mock(name="mocked_subprocess")
    mocker.patch("configsync.utils.subprocess", mocked_subprocess)
    return mocked_subprocess


class TestGit:
    def test_init_not_initialized(self, mock_subprocess, mocker, db_dir):

        mocker.patch("configsync.utils.os.path.exists", mocker.Mock(return_value=False))

        # dirty fix as the fixture tmp_path Path objects don't work when used
        # with str.join -> see Git._run_git method
        configsync.utils.DATABASE = str(db_dir)

        g = configsync.utils.Git()
        g.init()

        args, kwargs = mock_subprocess.run.call_args

        assert f"git init {configsync.utils.DATABASE}" in args

    def test_init_initialized(self, mock_subprocess, mocker, db_dir):

        mocker.patch("configsync.utils.os.path.exists", mocker.Mock(return_value=True))

        # dirty fix as the fixture tmp_path Path objects don't work when used
        # with str.join -> see Git._run_git method
        configsync.utils.DATABASE = str(db_dir)

        g = configsync.utils.Git()
        g.init()

        mock_subprocess.run.assert_not_called()

    def test_add(self, mock_subprocess, db_dir):

        configsync.utils.DATABASE = str(db_dir)
        g = configsync.utils.Git()
        g.add()

        args, kwargs = mock_subprocess.run.call_args
        assert f"git add ." in args

    def test_commit(self, mock_subprocess, mocker):

        mocked_datetime = mocker.Mock(name="mocked_datetime")
        mocked_datetime.now.return_value.strftime.return_value = "now"
        mocker.patch("configsync.utils.datetime.datetime", mocked_datetime)

        g = configsync.utils.Git()
        g.commit()

        args, kwargs = mock_subprocess.run.call_args

        assert f"git commit -m now" in args

    def test_checkout(self, mock_subprocess):

        g = configsync.utils.Git()
        g.checkout("timestamp")

        args, kwargs = mock_subprocess.run.call_args

        assert "git checkout timestamp" in args

    def test_push(self, mock_subprocess):

        g = configsync.utils.Git()
        g.push()

        args, kwargs = mock_subprocess.run.call_args

        assert "git push origin master" in args

    @pytest.mark.parametrize(
        "stdout, expected",
        [
            pytest.param("origin", True, id="remote->True"),
            pytest.param("", False, id="remote->False"),
        ],
    )
    def test_has_remote(self, stdout, expected, mock_subprocess):

        mock_subprocess.run.return_value.stdout = stdout

        g = configsync.utils.Git()

        assert g.has_remote == expected

    def test_set_remote(self, mock_subprocess, mocker):

        mocked_has_remote = mocker.PropertyMock(
            name="mocked_has_remote", return_value=False
        )
        mocker.patch("configsync.utils.Git.has_remote", new_callable=mocked_has_remote)

        g = configsync.utils.Git()
        g.set_remote("url_xy")

        args, kwargs = mock_subprocess.run.call_args

        assert "git remote add origin url_xy" in args

    @pytest.mark.parametrize(
        "from_remote, expected",
        [
            pytest.param(
                True,
                ("git fetch origin", "git reset --hard origin/master"),
                id="from_remote=True",
            ),
            pytest.param(False, ("git reset --hard master",), id="from_remote=False"),
        ],
    )
    def test_restore(self, from_remote, expected, mock_subprocess):

        g = configsync.utils.Git()
        g.restore(from_remote=from_remote)

        for call, exp_call in zip(mock_subprocess.run.call_args_list, expected):
            args, kwargs = call
            assert exp_call in args


class TestMainConfig:
    def test_add_remote_section(self, cs_dir):

        _, cs_dir, _ = cs_dir

        with configsync.utils.MainConfig() as main:
            main.add_remote_section()

        config = cs_dir / "CONFIG"

        assert "[remote]" in config.read_text()

    def test_set_remote_token(self, cs_dir):
        _, cs_dir, _ = cs_dir
        config = cs_dir / "CONFIG"
        config.write_text("[remote]")

        with configsync.utils.MainConfig() as main:
            main.set_remote_token(token="123")

        assert "[remote]\ntoken = 123" in config.read_text()

    def test_set_provider_token(self, cs_dir):

        _, cs_dir, _ = cs_dir
        config = cs_dir / "CONFIG"
        config.write_text("[remote]")

        with configsync.utils.MainConfig() as main:
            main.set_remote_repo("https://www.some.com")

        assert "[remote]\nrepo = https://www.some.com" in config.read_text()


def test_set_remote(mocker):

    mocked_Git = mocker.Mock(name="mocked_Git")
    mocker.patch("configsync.utils.Git", mocked_Git)

    configsync.utils.set_remote(
        "https://github.com/xibalba01/config-sync-remote.git", "abcd1234"
    )

    mocked_Git.return_value.set_remote.assert_called_with(
        remote="https://abcd1234@github.com/xibalba01/config-sync-remote.git"
    )

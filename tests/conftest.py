import pytest

import configsync


@pytest.fixture(autouse=True)
def cs_dir(tmp_path):

    user_dir = tmp_path / "user"
    user_dir.mkdir()

    cs_dir = user_dir / ".config-sync"
    cs_dir.mkdir()
    config = cs_dir / "CONFIG"
    config.touch()

    database = cs_dir / "database"
    database.mkdir()

    return user_dir, cs_dir, database


@pytest.fixture
def module_patcher(cs_dir, monkeypatch):
    """Patch config-sync path variables for a module under test

    It should be used at the top-level of a test_module with autouse enabled.

    Does not prevent config-sync from creating the real application folder
    - this still needs investigation.
    """
    userdir, cs_dir, database = cs_dir

    def _module_patcher(module):

        patches = (
            ("USER_DIR", userdir),
            ("CONFIG_SYNC_DIR", cs_dir),
            ("DATABASE", database),
        )

        for attr, patch in patches:
            try:
                monkeypatch.setattr(getattr(configsync, module), attr, patch)
            except AttributeError:  # setting not imported in local namespace
                continue

    return _module_patcher


@pytest.fixture(autouse=True)
def db_dir(cs_dir):
    _, _, database = cs_dir
    return database


@pytest.fixture
def app_config(db_dir):
    def _app_config(name):
        app_path = db_dir / name
        app_path.mkdir()
        app_cfg = app_path / "META"
        app_cfg.touch()
        app_cfg.write_text("[meta]\ntracked_files = path_abc")
        return app_cfg

    return _app_config

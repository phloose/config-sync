"""Unit tests for the configsync.core module"""
import configparser
import os

import pytest

import configsync.core


@pytest.fixture(autouse=True)
def patch(module_patcher):
    module_patcher("core")


@pytest.fixture
def db_app_path(db_dir):
    app_name = "app_xy"
    db_app_path = db_dir / app_name

    return db_app_path


@pytest.fixture
def meta_path(db_app_path):
    return db_app_path / "META"


@pytest.fixture
def tmp_file(tmp_path):
    def _tmp_file(file_name="some_file.cfg"):
        tmp_file = tmp_path / file_name
        tmp_file.touch()
        return tmp_file

    return _tmp_file


class TestLocalDatabase:
    def test_add_app(self, db_app_path, meta_path, tmp_file):
        """Tests if the application folder gets created in the database"""

        some_file = tmp_file()

        db = configsync.core.LocalDatabase()
        db.add(app_name="app_xy", app_file=str(some_file))

        cp = configparser.ConfigParser()
        cp.read_string(meta_path.read_text())

        assert db_app_path.exists()
        assert cp.get("some_file.cfg", "db_path") == str(
            db_app_path.joinpath(some_file.name)
        )
        assert cp.get("some_file.cfg", "orig_path") == os.path.abspath(some_file)
        assert db_app_path.joinpath("some_file.cfg").exists()

    @pytest.mark.parametrize(
        "filename",
        [
            pytest.param("some_other_file.cfg", id="app-already-in-db"),
            pytest.param(
                "some_file.cfg",
                id="app-already-in-db--dublicate-filename",
                marks=pytest.mark.xfail,
            ),
        ],
    )
    def test_add_app_exists(self, filename, db_app_path, meta_path, tmp_file):

        some_file = tmp_file(filename)
        db_app_path.mkdir()
        meta_path.touch()
        meta_path.write_text("[some_file.cfg]\ndb_path = xyz\norig_path = abc")

        db = configsync.core.LocalDatabase()
        db.add(app_name="app_xy", app_file=str(some_file))

        cp = configparser.ConfigParser()
        cp.read_string(meta_path.read_text())

        assert filename in cp.sections()

    def test_remove_app(self, db_app_path, meta_path):

        db_app_path.mkdir()

        db = configsync.core.LocalDatabase()
        db.remove(app_name="app_xy")

        assert not db_app_path.exists()

    def test_remove_file_from_app(self, db_app_path, meta_path):

        # build up database structure for a sample app -
        db_app_path.mkdir()
        some_app_file = db_app_path / "some_file.cfg"
        some_app_file.touch()

        meta_path.touch()
        meta_path.write_text(f"[{some_app_file}]\ndb_path = abc\norig_path = xyz")

        db = configsync.core.LocalDatabase()
        db.remove(app_name="app_xy", app_file="some_file.cfg")

        assert "some_file.cfg" not in open(meta_path).read()
        assert not some_app_file.exists()

    def test_list_apps(self, db_dir, tmp_file):

        paths = ["app_a", "app_b", "app_c"]
        for path in paths:
            app_path = db_dir / path
            app_path.mkdir()

        db = configsync.core.LocalDatabase()
        ret = db.list()

        assert ret == paths

    def test_list_app_files(self, db_dir, tmp_file):

        db = configsync.core.LocalDatabase()
        files = ["file_a.cfg", "file_b.cfg", "file_c.cfg"]
        for file in files:
            app_file = tmp_file(file)
            db.add(app_name="app_xy", app_file=app_file)

        db = configsync.core.LocalDatabase()
        ret = db.list(app_name="app_xy")

        assert ret == files

    @pytest.mark.parametrize(
        "to_remote",
        [
            pytest.param(False, id="to_remote->False"),
            pytest.param(True, id="to_remote->True"),
        ],
    )
    def test_backup(self, to_remote, mocker):

        mocked_Git = mocker.Mock(name="mocked_Git")
        mocker.patch("configsync.core.Git", mocked_Git)

        db = configsync.core.LocalDatabase()
        db.backup(to_remote=to_remote)

        mocked_Git.return_value.add.assert_called()
        mocked_Git.return_value.commit.assert_called()

        if to_remote:
            mocked_Git.return_value.push.assert_called()

    def test_restore_source_existing(self, tmp_file, db_dir, mocker):

        # TODO: Whats the value of this test? To see if existing source files don't
        #  crash config-sync?

        mocked_git = mocker.Mock(name="mocked_git")
        mocker.patch("configsync.core.Git", mocker.Mock(return_value=mocked_git))

        # add 3 apps with each 2 files to the database
        src_filepaths = []
        for app in ["a", "b", "c"]:
            for file in ["file1.txt", "file2.txt"]:
                new_file = tmp_file(f"{app}_{file}")
                src_filepaths.append(new_file)
                new_file.touch()
                configsync.core.LocalDatabase().add(app_name=app, app_file=new_file)

        db = configsync.core.LocalDatabase()
        db.restore()

        mocked_git.restore.assert_called()
        assert all(file.exists() for file in src_filepaths)

    def test_restore_source_not_existing(self, tmp_file, db_dir, mocker):

        mocked_git = mocker.Mock(name="mocked_git")
        mocker.patch("configsync.core.Git", mocker.Mock(return_value=mocked_git))

        # add 3 apps with each 2 files to the database
        src_filepaths = []
        for app in ["a", "b", "c"]:
            for file in ["file1.txt", "file2.txt"]:
                new_file = tmp_file(f"{app}_{file}")
                src_filepaths.append(new_file)
                new_file.touch()
                configsync.core.LocalDatabase().add(app_name=app, app_file=new_file)

        # now pretend to be a new system so the source folder needs to be empty
        for file in src_filepaths:
            file.unlink()

        db = configsync.core.LocalDatabase()
        db.restore()

        mocked_git.restore.assert_called()
        assert all(file.exists() for file in src_filepaths)

    def test_db_integrity_clean(self, tmp_path, db_dir):
        # For a used app that is tracked by config-sync the integrty check
        # should return True.
        # _check_origins only returns True if ALL tracked files have a real
        # counter part (orig_path exists) and exist themselfs in the database
        # (db_path exists)
        db = configsync.core.LocalDatabase()
        for app in ["a", "b", "c"]:
            app_folder = tmp_path / app
            app_folder.mkdir()

            for file in ["1.txt", "2.txt", "3.txt"]:
                filename = f"{app}_{file}"
                filepath = app_folder.joinpath(filename)
                filepath.touch()

                db.add(app_name=app, app_file=filepath)

        assert db._db_integrity()

    def test_db_integrity_dirty_source(self, tmp_path, db_dir):
        # For a used app that is tracked by config-sync the integrty check
        # should return True.
        # _check_origins only returns True if ALL tracked files have a real
        # counter part (orig_path exists) and exist themselfs in the database
        # (db_path exists)
        db = configsync.core.LocalDatabase()
        for app in ["a", "b", "c"]:
            app_folder = tmp_path / app
            app_folder.mkdir()

            for file in ["1.txt", "2.txt", "3.txt"]:
                filename = f"{app}_{file}"
                filepath = app_folder.joinpath(filename)
                filepath.touch()

                db.add(app_name=app, app_file=filepath)

        # remove a source file to pretend faulty integrity
        error_a = tmp_path / "a" / "a_1.txt"
        error_a.unlink()

        assert not db._db_integrity()

    def test_db_integrity_dirty_db(self, tmp_path, db_dir):
        # For a used app that is tracked by config-sync the integrty check
        # should return True.
        # _check_origins only returns True if ALL tracked files have a real
        # counter part (orig_path exists) and exist themselfs in the database
        # (db_path exists)
        db = configsync.core.LocalDatabase()
        for app in ["a", "b", "c"]:
            app_folder = tmp_path / app
            app_folder.mkdir()

            for file in ["1.txt", "2.txt", "3.txt"]:
                filename = f"{app}_{file}"
                filepath = app_folder.joinpath(filename)
                filepath.touch()

                db.add(app_name=app, app_file=filepath)

        # remove a database file to pretend faulty integrity
        error_db = db_dir / "a" / "a_1.txt"
        error_db.unlink()

        assert not db._db_integrity()


class TestAppConfig:
    def test_create(self, db_app_path):

        app_name = "app_xy"
        db_app_path.mkdir()

        cfg = configsync.core.AppConfig(app_name)
        cfg.create()

        assert all("META" in str(path) for path in db_app_path.iterdir())

    def test_add_file_section(self, db_app_path, meta_path, tmp_file):

        file = tmp_file()

        app_name = "app_xy"
        db_app_path.mkdir()
        meta_path.touch()
        db_app_file = db_app_path / file.name

        with configsync.core.AppConfig(app_name) as cfg:
            cfg.add_file_section(
                file.name, db_path=str(db_app_file), orig_path=str(file)
            )

        result = open(meta_path).read()

        assert file.name in result
        assert str(db_app_file) in result
        assert str(file) in result

# Changelog


## [0.1.0-a1] - 2020-01-15

### Fixed

- ``subprocess.run`` with parameter ``capture_output`` fails in python < 3.7. This has been fixed by using ``stdout=subprocess.PIPE`` and ``stderr=subprocess.PIPE``.


## [0.1.0-a0] - 2019-12-09

### Added

- Core components:
  - ``LocalDatabase``: Contains the logic for the main functionality of config-sync
  - ``MainConfig``: Helper class for interacting with config-sync's main configuration
  - ``AppConfig``: Helper class for interacting with configuration for single applications
  - ``Git``: An interface for interacting with git.
  - Exceptions: ``ConfigSyncError``, ``ConfigError``, ``MainConfigError``, ``AppConfigError``, ``GitError``, ``GitCmdError``
- A set of helper functions for initializing config-sync on a new system and setting different configuration parameters (``init``, ``setup``, ``set_opt``, ``set_remote``)
- A commandline parsing configuration based on ``argparse``.
- A helper class ``CmdAction`` for connecting functions/methods with commandline arguments
- ``logging`` for displaying user or debugging information

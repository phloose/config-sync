"""config-sync core functionality"""
import configparser
import logging
import os
import shutil

from configsync.exceptions import AppConfigError, GitCmdError
from configsync.settings import DATABASE
from configsync.utils import Git

logger = logging.getLogger("config-sync.core")


class AppConfig(configparser.ConfigParser):
    """Access the configuration file for a specific app

    Should be used as a context manager to facilitate writing changes to META.
    """

    def __init__(self, app_name):
        super().__init__()
        self.app_name = app_name
        self.app_path = os.path.join(DATABASE, self.app_name)
        self.meta_path = os.path.join(self.app_path, "META")
        self.read()

    def create(self):
        """Create an app config file "META"
        """
        if not os.path.exists(self.meta_path):
            with open(self.meta_path, "w") as meta:
                self.write(meta, space_around_delimiters=False)

    def add_file_section(self, section, db_path, orig_path):
        """Add a config section for a tracked app file

        Args:
            section (str): Section name, which is a files basename
            db_path (str): Absolute path of the file in the database
            orig_path (str): Absolute path to the original file location
        """
        self.add_section(section)
        self.set(section, "db_path", db_path)
        self.set(section, "orig_path", orig_path)

    def read(self):
        """Read the META file
        """
        super().read(self.meta_path, encoding=None)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, value, traceback):
        """Always write to the meta file after the with block exists
        """
        if exc_type is not None:
            if issubclass(exc_type, configparser.Error):
                raise AppConfigError(value, exc_type, traceback)
            else:
                return False
        with open(self.meta_path, "w") as cfg:
            self.write(cfg)


class LocalDatabase:
    def __init__(self):
        self.path = DATABASE

    def add(self, app_name, app_file):
        """Add a new file to the database

        Args:
            app_name (str): Name for the app entry
            app_file (str): Path to the new file
        """
        orig_path = os.path.abspath(app_file)
        filename = os.path.basename(app_file)

        db_app_dir = os.path.join(self.path, app_name)
        db_app_file = os.path.join(db_app_dir, filename)

        try:
            os.mkdir(db_app_dir)
        except FileExistsError:
            pass
        else:
            logger.info(f"Created new backup folder for {app_name!r}")

        try:
            os.link(orig_path, db_app_file)
        except OSError as e:
            logger.debug(e, exc_info=True)
            logger.info(
                f"An error occured while linking the new file into the database: {e}"
            )
        else:
            with AppConfig(app_name) as ac:
                ac.add_file_section(filename, db_app_file, orig_path)
            logger.info(f"Added a new file to {app_name!r}")

    def remove(self, app_name, app_file=None):
        """Remove a whole app or a single setting from the database

        Args:
            app_name (str): The app to remove
            app_file (str, optional): The app file to remove. Defaults to None.
        """
        db_path = os.path.join(self.path, app_name)
        if app_file is None:
            try:
                shutil.rmtree(db_path)
                logger.info(f"Removed {app_name!r} from the database")
                return
            except OSError as e:
                logger.debug(e, exc_info=True)
                logger.info(f"An error occured while removing {app_name!r}: {e}")
                return
        app_file = os.path.join(db_path, app_file)
        try:
            os.remove(app_file)
        except OSError as e:
            logger.debug(e, exc_info=True)
            logger.info(
                f"An error occured while removing"
                f" {app_file!r} from {app_name!r}: {e}"
            )
        else:
            with AppConfig(app_name) as ac:
                ac.remove_section(app_file)
            logger.info(f"Successfully removed {app_file!r} from {app_name}")

    def list(self, app_name=None):
        """List all apps in the database or the files of a specific app

        Args:
            app_name (str, optional): The name of an app. Defaults to None.

        Returns:
            list: A list containing all apps in the database or all files for an app
        """
        if app_name is None:
            return sorted(filter(lambda f: ".git" not in f, os.listdir(DATABASE)))
        with AppConfig(app_name) as ac:
            return sorted(ac.sections())

    def backup(self, to_remote=False):
        """Backup the database

        Args:
            to_remote (bool, optional): Backup to a remote repo. Defaults to False.
        """
        if not self._db_integrity():
            return
        g = Git()
        try:
            g.add()
            g.commit()
            if to_remote:
                g.push()
        except GitCmdError as e:
            logger.debug(e, exc_info=True)
            logger.info(f"An error occured during backup: {e}")
        else:
            logger.info("Backup successful!")

    def restore(self, from_remote=False):
        """Restore the database

        Args:
            from_remote (bool, optional): Restore from a remote repo. Defaults to False.
        """
        g = Git()
        try:
            g.restore(from_remote=from_remote)
        except GitCmdError as e:
            logger.debug(e, exc_info=True)
            logger.info(f"Restoring the database failed: {e}")
        else:
            logger.info("Database restored to last backup")
        for app in self.list():
            with AppConfig(app) as ac:
                for file in ac.sections():
                    orig_path = ac.get(file, "orig_path")
                    db_path = ac.get(file, "db_path")
                    if not os.path.exists(os.path.dirname(orig_path)):
                        logger.info(
                            f"Path for {file!r} ({orig_path!r}) not found. Skipping!"
                        )
                        continue
                    try:
                        os.link(db_path, orig_path)
                    except FileExistsError as e:
                        logger.debug(e, exc_info=True)
                        os.remove(orig_path)
                        os.link(db_path, orig_path)
                        logger.info(f"Restored {file} successful to {orig_path}")
                    except PermissionError as e:
                        logger.debug(e, exc_info=True)
                        logger.info(
                            f"No permission for restoring {file!r} to {db_path!r}!"
                        )

    def _entry_integrity(self, config, entry):
        """Check if the paths of a config entry exist

        Args:
            config (AppConfig): An AppConfig instance for a specifc app
            entry (str): The entry name, which is a filename

        Returns:
            tuple: A tuple containing two booleans that indicate whether the original
                   path and the database path of an entry exists.
        """
        orig_path = config.get(entry, "orig_path")
        db_path = config.get(entry, "db_path")
        orig_exists = os.path.exists(orig_path)
        db_exists = os.path.exists(db_path)
        return orig_exists, db_exists

    def _app_integrity(self, app_name):
        """Check if all paths for an app exist

        Args:
            app_name (str): An apps name

        Returns:
            bool: Whether all original paths and all database paths for an app exist
        """
        orig_checked = []
        db_checked = []
        with AppConfig(app_name) as ac:
            for file in ac.sections():
                orig, db = self._entry_integrity(ac, file)
                if not orig:
                    logger.info(
                        f"The original path for {file!r} of {app_name!r} "
                        f"does not exist: {ac.get(file, 'orig_path')!r}"
                    )
                if not db:
                    logger.info(
                        f"The database path for {file!r} of {app_name!r} "
                        f"does not exist: {ac.get(file, 'db_path')!r}"
                    )
                orig_checked.append(orig)
                db_checked.append(db)
            return all(orig_checked) and all(db_checked)

    def _db_integrity(self):
        """Check if all paths (original and database) for the whole database exist

        Returns:
            bool: Whether all paths exist or not
        """
        if not all(self._app_integrity(entry) for entry in self.list()):
            logger.info(
                "Paths need to be fixed before backing up!\n"
                "You could:\n"
                "\t- manually edit the respective path in the apps META file\n"
                "\t- remove the app's setting (or the whole app))\n"
            )
            return False
        return True

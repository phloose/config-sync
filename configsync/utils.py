"""config-sync utils"""
import configparser
import logging
import os
import subprocess
import datetime

from configsync.exceptions import GitCmdError, MainConfigError
from configsync.settings import CONFIG_SYNC_DIR, DATABASE

logger = logging.getLogger("config-sync.utils")


class Git:
    """A Proxy for calls to git via subprocess.run"""

    def __init__(self):
        self.repo = DATABASE

    def _run_git(self, cmd, *args):
        """Execute a git subcommand with optional arguments

        Should only be used inside this class.

        Args:
            cmd (str): A git subcommand (e.g. add or commit)

        Raises:
            GitCmdError: When a git command fails a GitCmdError is raised

        Returns:
            subprocess.CompletedProcess: An object containing detailed information about
                the executed command.
        """
        try:
            os.chdir(self.repo)
            cmd = subprocess.run(
                f'git {cmd} {" ".join(args)}',
                shell=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
            )
            cmd.check_returncode()
        except subprocess.CalledProcessError as e:
            raise GitCmdError(e.cmd, e.returncode, e.stderr) from None
        else:
            return cmd

    def init(self):
        """Initialize a git repository"""
        git_dir = os.path.join(DATABASE, ".git")
        if not os.path.exists(git_dir):
            self._run_git("init", DATABASE)

    def add(self):
        """Add all files to the index"""
        self._run_git("add", ".")

    def commit(self):
        """Make a commit with the current timestamp"""
        now = datetime.datetime.now()
        timestamp = now.strftime("%d-%m-%Y_%H%M%S")
        self._run_git("commit", "-m", timestamp)

    def checkout(self, commit):
        """Checkout specific commit"""
        self._run_git("checkout", commit)

    def push(self):
        """Push local master branch to repository that is stored under origin"""
        self._run_git("push", "origin", "master")

    def restore(self, from_remote=False):
        """Restores the state of the repo either from the last state of the local or
        remote master branch.

        All previous changes, if not backed up, will be lost because `git reset --hard`
        will be used.

        Args:
            from_remote (bool, optional): Whether to restore from remote.
                Defaults to False.
        """
        if from_remote:
            self._run_git("fetch", "origin")
            self._run_git("reset", "--hard", "origin/master")
        else:
            self._run_git("reset", "--hard", "master")

    @property
    def has_remote(self):
        """Checks if origin is already set

        Returns:
            bool: True if origin is set else False
        """
        cmd = self._run_git("remote", "-v")
        if "origin" in str(cmd.stdout):
            return True
        return False

    def set_remote(self, remote):
        """Set the remote repository address.

        At first it will be tried to add origin. If that fails it will be reset.

        Args:
            remote (str): Remote repository address. Should be https or ssh.
        """
        try:
            self._run_git("remote", "add", "origin", remote)
        except GitCmdError as e:
            logger.debug(e, exc_info=True)
            self._run_git("remote", "set-url", "origin", remote)


class MainConfig(configparser.ConfigParser):
    """A specialization of configparser.ConfigParser for reading and accessing the
    configsync main configuration file.

    Should be used as a context manager to ensure that all changes made to a config are
    written back to file.
    """

    def __init__(self):
        super().__init__()
        self.path = os.path.join(CONFIG_SYNC_DIR, "CONFIG")
        self.read()

    def read(self):
        """Read the main configuration file

        Returns:
            list: A list containing successfully read files
        """
        return super().read(self.path, encoding=None)

    def add_remote_section(self):
        """Add a remote section"""
        self.add_section("remote")

    def set_remote_token(self, token):
        """Set the personal access token option in the remote section

        Args:
            token (str): A personal access token from github
        """
        self.set(f"remote", "token", token)

    def set_remote_repo(self, repo):
        """Set the remote repository address option in the remote section

        Args:
            repo (str): The address of the remote repository
        """
        self.set(f"remote", "repo", repo)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, value, traceback):
        if exc_type is not None:
            if issubclass(exc_type, configparser.Error):
                raise MainConfigError(value, exc_type, traceback) from None
            else:
                return False
        with open(self.path, "w") as main:
            self.write(main)


def set_remote(url, token=None):
    """Sets the git remote repository.

    When given a personal access token and a https url it inserts that token into the
    address in the form https://token@github.com/user/repo.git. Otherwise it just sets
    the given url as git remote.

    Args:
        url (str): Url to a git remote repository (https or ssh)
        token (str, optional): A personal access token. Defaults to None.
    """
    if token:
        import re

        match = re.match(r"(https?:\/\/)*?((github|gitlab).com.*)", url)
        if match:
            first, last, *rest = match.groups()
            remote = f"{first}{token}@{last}"
            Git().set_remote(remote=remote)
            return
    Git().set_remote(remote=url)


def setup():
    """Setup the configsync directory structure and an empty main config"""
    import os

    try:
        os.mkdir(CONFIG_SYNC_DIR)
    except Exception as e:
        logger.error(e)
        return
    else:
        try:
            os.mkdir(DATABASE)
        except Exception as e:
            logger.error(e)
            return
        else:
            import pathlib

            main_config = os.path.join(CONFIG_SYNC_DIR, "CONFIG")
            pathlib.Path(main_config).touch()


def init():
    """Initialize the configsync directory structure and database"""
    import os

    if not os.path.exists(CONFIG_SYNC_DIR):
        setup()

    git_dir = os.path.join(DATABASE, ".git")
    if not os.path.exists(git_dir):
        Git().init()


def set_opt(repo=None, token=None):
    """Set options like repository or access token in the main configuration

    This is called from the cli config subcommand.

    Args:
        repo (str, optional): A repository url. Defaults to None.
        token (str, optional): A personal access token. Defaults to None.
    """
    with MainConfig() as config:
        try:
            config.add_remote_section()
        except configparser.DuplicateSectionError:
            pass  # just try to add a section - ignore if it exists
        if token:
            config.set_remote_token(token=token)
        if repo:
            config.set_remote_repo(repo=repo)

        if config.has_option("remote", "repo"):
            remote_repo = config.get("remote", "repo")
            if remote_repo.startswith("git@"):
                set_remote(remote_repo)
            elif remote_repo.startswith("https://") and config.has_option(
                "remote", "token"
            ):
                set_remote(remote_repo, config.get("remote", "token"))
            else:
                logger.info(
                    "Could not set remote! Set a valid repo (ssh or https "
                    "address) and, if using https, set an access token!"
                )

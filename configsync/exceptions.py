"""config-sync exceptions"""

# General


class ConfigSyncError(Exception):
    """Base Exception for all config-sync related errors. Not to be raised
    directly"""

    pass


# Config


class ConfigError(ConfigSyncError):
    """Raised for all config related errors"""

    def __init__(self, msg, origin, traceback=None):
        self.msg = msg
        self.origin = origin
        self.traceback = traceback

    def __str__(self):
        return f"{self.msg!r} emitted from {self.origin!r}"


class MainConfigError(ConfigError):
    """Raised for errors in the MainConfig"""

    pass


class AppConfigError(ConfigError):
    """Raised for errors in the AppConfig"""

    pass


# Git


class GitError(ConfigSyncError):
    """Base error for all git related errors"""

    pass


class GitCmdError(GitError):
    """Raised when a git command fails"""

    def __init__(self, cmd, returncode, stderr):
        self.cmd = cmd
        self.returncode = returncode
        self.stderr = stderr

    def __str__(self):
        err_msg = (
            f"{self.cmd!r} returned the following error: "
            f"{str(self.stderr, 'utf-8')!r}. Exitcode: {self.returncode}"
        )
        return err_msg

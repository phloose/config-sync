"""config-sync cli"""
import sys

import configsync
from configsync.cli.parsers import parser
from configsync.cli.utils import CmdAction, cli_list
from configsync.core import LocalDatabase
from configsync.utils import init, set_opt


def parse_args(args=sys.argv[1:]):
    if "--debug" in args:
        configsync.logger.setLevel(configsync.logging.DEBUG)
    if len(args) == 0 and "--debug" not in args:
        parser.print_help()

    parsed = parser.parse_args(args)

    actions = CmdAction(parsed)
    actions.add_multiple(
        [
            ("add", LocalDatabase().add, ["app_name", "app_file"]),
            ("remove", LocalDatabase().remove, ["app_name", "app_file"]),
            ("list", cli_list, ["app_name"]),
            ("backup", LocalDatabase().backup, ["to_remote"]),
            ("restore", LocalDatabase().restore, ["from_remote"]),
            ("init", init, []),
            ("config", set_opt, ["token", "repo"]),
        ]
    )

    if parsed.cmd:
        actions.do(parsed.cmd)


if __name__ == "__main__":
    parse_args()

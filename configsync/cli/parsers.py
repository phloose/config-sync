"""config-sync cli parsers setup"""
import argparse

common_args = argparse.ArgumentParser(add_help=False)
common_args.add_argument(
    "--debug", action="store_true", default=False, help="Toggle debug output"
)

parser = argparse.ArgumentParser(
    prog="config-sync",
    description="config-sync | easily sync your config across multiple devices",
    parents=[common_args],
)


subparser = parser.add_subparsers(dest="cmd")

config = subparser.add_parser("config", parents=[common_args])
config.add_argument("--provider", dest="provider")
config.add_argument("--token", dest="token")
config.add_argument("--repo", dest="repo")
config.add_argument("--get", dest="query", choices=["token", "repo", "provider"])

add_desc = """Add setting to database"""
add_setting = subparser.add_parser("add", parents=[common_args], help=add_desc)
add_setting.add_argument("--name", "-n", dest="app_name", required=True)
add_setting.add_argument("--file", "-f", dest="app_file", required=True)

remove_desc = """Remove setting / stop tracking a setting"""
remove_settings = subparser.add_parser(
    "remove", parents=[common_args], help=remove_desc
)
remove_settings.add_argument("--name", "-n", dest="app_name", required=True)
remove_settings.add_argument(
    "--file", "-f", dest="app_file", required=False, default=None
)

list_desc = """List tracked settings"""
list_settings = subparser.add_parser("list", parents=[common_args], help=list_desc)
list_settings.add_argument(
    "--name", "-n", dest="app_name", required=False, default=None
)

backup_desc = """Backup the database"""
backup_settings = subparser.add_parser(
    "backup", parents=[common_args], help=backup_desc
)
backup_settings.add_argument(
    "--remote",
    "-r",
    dest="to_remote",
    required=False,
    action="store_true",
    default=False,
)

restore_desc = """Restore all settings from the current state of the database"""
restore_settings = subparser.add_parser(
    "restore", parents=[common_args], help=restore_desc
)
restore_settings.add_argument(
    "--remote",
    "-r",
    dest="from_remote",
    required=False,
    action="store_true",
    default=False,
)

init_desc = """Initialize config-sync"""
init_settings = subparser.add_parser("init", parents=[common_args], help=init_desc)
init_settings.add_argument("--remote", "-r", required=False, action="store_true")

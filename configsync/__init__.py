"""config-sync"""
import logging

from pkg_resources import get_distribution, DistributionNotFound

from configsync.settings import userinfo, verbose

try:
    __version__ = get_distribution("config-sync").version
except DistributionNotFound:
    __version__ = "unknown"

logger = logging.getLogger("config-sync")
logger.setLevel(logging.INFO)
logger.addHandler(userinfo)
logger.addHandler(verbose)

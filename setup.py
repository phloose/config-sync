from setuptools import setup, find_packages

with open("README.md") as readme_file:
    long_description = readme_file.read()

with open("CHANGELOG.md") as changelog_file:
    changes = changelog_file.read()

setup(
    name="config-sync",
    author="Philipp Loose",
    author_email="xibalba@disroot.org",
    url="https://gitlab.com/phloose/config-sync/",
    description="A tool for backup and restore of configuration files",
    long_description=long_description + "\n\n" + changes,
    keywords="config config-sync backup restore sync remote git",
    license="MIT",
    packages=find_packages(exclude=["tests"]),
    entry_points={"console_scripts": ["config-sync = configsync.cli:parse_args"]},
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Intended Audience :: System Administrators",
        "Intended Audience :: End Users/Desktop",
        "Topic :: System :: Archiving :: Backup",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Operating System :: Microsoft :: Windows :: Windows 10",
        "Operating System :: POSIX :: Linux",
    ],
    python_requires=">=3.6",
    extras_require={
        "dev": [
            "pytest>=5.1.0",
            "pytest-cov>=2.7.1",
            "pytest-mock>=1.10.4",
            "flake8>=3.7.8",
            "black>=19.3b0",
        ]
    },
    setup_requires=["setuptools_scm"],
    use_scm_version=True,
    project_urls={"Bug Reports": "https://gitlab.com/phloose/config-sync/issues"},
)

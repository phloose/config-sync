# config-sync

-   [Motivation](#motivation)
-   [Installation & requirements](#installation-requirements)
-   [Usage](#usage)
    -   [Basic usage:](#basic-usage)
-   [Contribution & Development](#contribution-development)

## Motivation

`config-sync` was developed out of need for the repetitive task of always backing up
your config files for setting up a new system or just for the sake of having a specific
configuration setting backed up.

The main goal is to have your config files saved at one place and still be
able use them normally in your applications. If any change happens it will be tracked,
can be backed up and restored (from a local or remote repository) with simple commands.
The difference to a [dot-file-repository](https://www.atlassian.com/git/tutorials/dotfiles)
is that you can choose which applications files you want to track even if they are not
in the home directory. While also using [git](https://git-scm.com/) in the backend it
abstracts a lot of the necessary commands to achieve the same result plus using hardlinks
between the original and the repository path of the tracked application files.

There are surely more sophisticated solutions for backing up configuration files. This is
mainly a hobby and learning project that fits my personal needs. I am always annoyed
when i need to copy all that configuration files from an old system to a new system or
just back them up when i screw something.

_tba_

## Installation & requirements

`config-sync` requires [git](https://git-scm.com/) to be installed.

For installation download or clone this repository, change into that directory and
install `config-sync` via pip:

```bash
cd config-sync
pip install .
```

A release on [pypi](https://pypi.org/) is planned.

## Usage

`config-sync` is a commandline application. There are five main commands:

-   add
-   remove
-   list
-   backup
-   restore

For every command help can be shown with `config-sync <command> -h` or `--help`

When used the first time you need to initialize config-sync via `config-sync init`. This
will create a _.config-sync_ folder in your home directory and initialize a git
repository in _.config-sync/database/_. This is where the tracked files will reside.

To be able to backup your database to a remote repository you should create a repository
and a personal access token with scope _repo_ on
[github](https://help.github.com/en/github/authenticating-to-github/creating-a-personal-access-token-for-the-command-line)
(gitlab should also work but is not tested yet).

Then run:

`config-sync config --token 1234 --repo https://github.com/<USERNAME>/<MYREPO.git>`

If you have already configured ssh you only need to add the repo url:

`config-sync config --repo git@github.com/<USERNAME>/<MYREPO.git>`

This way you can always change your repo or token.

You should keep a seperate repo just for your config-files for one operating system type.
Backing up and syncing between e.g. windows and linux does not work as the paths of the
tracked files are tracked too and thus can't be resolved between the two operating systems.

### Basic usage:

-   To add a file to the database run `config-sync add -n <appname> -f <filepath>`.

-   Removing a tracked file from the database can be done via
    `config-sync remove -n <appname> -f <filename>`. For completly removing an app
    omit the `-f` option.

-   To backup you databse run `config-sync backup` and to restore `config-sync restore`.
    Both commands accept a `-r/--remote` option that backup to/restore from a remote
    repository.

-   To see whichs apps are in the database run `config-sync list` or if you want to know
    which files are tracked of a specific app run `config-sync list -n <appname>`.

Note: When files are added they are **NOT** backed up already. To do this explicitly
invoke the backup command. Take care of this otherwise a restore might simply overwrite
these changes without asking.

## Contribution & Development

This is heavily work in progress and not stable at all. Any contribution, reviews,
manual testing, bugs etc. is greatly appreciated!

To install `config-sync` in edit-mode with development dependencies run:

`pip install -e .[dev]`

For testing pytest is used. For every new change there should be at least one test.
